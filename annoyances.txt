[Adblock Plus 2.0]
! Version:
! Title: Capi Etheriel Annoyances list
! Last modified:
! Expires: 4 days (update frequency)
! Homepage: https://gitlab.com/barraponto/adblock-lists/
! 
! removes annoyances in the content of lifehacker.com and all of kinja
lifehacker.com###sharingfooter
lifehacker.com##aside.inset--story
lifehacker.com##.instream-native-video
avclub.com###sharingfooter
avclub.com##aside.inset--story
avclub.com##.instream-native-video
deadspin.com###sharingfooter
deadspin.com##aside.inset--story
deadspin.com##.instream-native-video
gizmodo.com###sharingfooter
gizmodo.com##aside.inset--story
gizmodo.com##.instream-native-video
jalopnik.com###sharingfooter
jalopnik.com##aside.inset--story
jalopnik.com##.instream-native-video
jezebel.com###sharingfooter
jezebel.com##aside.inset--story
jezebel.com##.instream-native-video
kotaku.com###sharingfooter
kotaku.com##aside.inset--story
kotaku.com##.instream-native-video
splinternews.com###sharingfooter
splinternews.com##aside.inset--story
splinternews.com##.instream-native-video
thetakeout.com###sharingfooter
thetakeout.com##aside.inset--story
thetakeout.com##.instream-native-video
theroot.com###sharingfooter
theroot.com##aside.inset--story
theroot.com##.instream-native-video
theonion.com###sharingfooter
theonion.com##aside.inset--story
theonion.com##.instream-native-video
clickhole.com###sharingfooter
clickhole.com##aside.inset--story
clickhole.com##.instream-native-video
theinventory.com###sharingfooter
theinventory.com##aside.inset--story
theinventory.com##.instream-native-video

! removes annoying videos in the middle of articles from newyorker.com
newyorker.com##[data-cne-interlude]